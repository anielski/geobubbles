DEP_DIR=poly2tri
MAKE=make
RM=rm

all:
	@$(MAKE) -C $(DEP_DIR)

clean:
	@$(MAKE) -C $(DEP_DIR) clean
	$(RM) -rf *.so *.pyc *~

.PHONY: all clean
