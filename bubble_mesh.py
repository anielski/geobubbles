__author__ = 'Piotr Anielski'

import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from scipy.spatial.distance import euclidean
import p2t
import math
import itertools

class BubbleMesh(object):

    def __init__(self, max_iter=300, lower_ratio=4.0, upper_ratio=10.0, delta_t=0.05, friction=0.4, mass=0.6):
        self.current_iter = 0
        self.max_iter = max_iter        #300
        self.friction = friction        #0.4
        self.mass = mass                #0.6
        self.dt = delta_t               #0.05
        self.upper_ratio = upper_ratio  #10.0
        self.lower_ratio = lower_ratio  #4.0
        self.pts_inside = []
        self.pts_border = []
        self.forces = []
        self.velocities = []
        self.forces = []
        self.polygon = None
        self.romb = None
        self.fig = plt.figure(figsize=(10,10))
        self.ax = self.fig.add_subplot(111, autoscale_on=True, xlim=(-5,15), ylim=(-5,15))
        plt.axis('equal')

    def set_poly(self, poly):
        self.pts_inside = []
        self.pts_border = []
        self.polygon = poly
        for pt in poly:
            self.pts_border.append(pt)
#        self.fig = plt.figure(figsize=(10,10))
#        self.ax = self.fig.add_subplot(111, autoscale_on=False, xlim=(-5,15), ylim=(-5,15))
#        plt.axis('equal')

    def show(self):
        poly_patches = mpatches.Polygon(self.polygon, alpha=0.1, color='green')
        self.ax.add_patch(poly_patches)

        #draw bubbles inside
        for pt in self.pts_inside:
            bubble = mpatches.Circle(pt, uniform_size(pt)/2, alpha=0.1) #add to circle collection
            self.ax.add_patch(bubble)
        # draw bubbles on edges
        for pt in self.pts_border:
            bubble = mpatches.Circle(pt, uniform_size(pt)/2, alpha=0.1) #add to circle collection
            self.ax.add_patch(bubble)
        # draw vectors from bubble motion
        if len(self.pts_inside) == 0:
            allpts = self.pts_border
        else:
            allpts = np.concatenate((self.pts_border, self.pts_inside), axis=0)
        for (point, force) in itertools.izip(allpts, self.forces):
           arrow = mpatches.Arrow(point[0], point[1], force[0], force[1], width=0.3)
           self.ax.add_patch(arrow)
#        self.show_triangulation()
#        plt.savefig("alpaka" + '{0:04}'.format(i) + ".png")
#        plt.cla()
#        plt.show()

    def next_step(self, adapt=True):
        '''
        funkcja wykonujaca algorytm z podzialem na kroki
        '''
        if self.current_iter == 0:

            plt.cla()
            self.mesh_edges()
            self.mesh_face()
            self.print_stats()
            if adapt:
                self.adapt_population()
            self.show()
            self.show_triangulation()

            plt.ion()
            plt.show()

            print "current iteration:", self.current_iter
            print "patches", len(self.ax.patches)
            self.current_iter += 1
            self.ax.figure.canvas.draw()

            return False

        if self.current_iter > self.max_iter:
            print "ending curr_iter", self.current_iter
            return True

        plt.cla()
        print "current iteration:", self.current_iter
        

        self.compute_bubbles_motion()
        self.move_bubbles()
        if adapt:
            self.adapt_population()

        self.show()
        self.show_triangulation()

        self.ax.figure.canvas.draw()
        # fix me: teraz trzeba to wywolac dwa razy
        self.ax.figure.canvas.draw()

        self.current_iter += 1
        return False


    def mesh(self, adapt=True):
        '''
        funkcja wykonujaca algorytm bez podzialu na kroki
        '''
        self.mesh_edges()
        self.mesh_face()

        self.print_stats()

        self.current_iter = 0
        if adapt:
            self.adapt_population()

        while self.current_iter < self.max_iter:
            self.compute_bubbles_motion()
            self.move_bubbles()
            if adapt:
                self.adapt_population()
            print "current iteration:", self.current_iter
            self.current_iter += 1

        self.show()
        self.show_triangulation()
        plt.show()


    def print_stats(self):
        print "nb of bubbles inside:",len(self.pts_inside)
        print "nb of bubbles on border:",len(self.pts_border)
        print "nb of bubbles from initial polygon:",len(self.polygon)


    def mesh_edges(self):
        for i in range(len(self.polygon)):
            j = (i + 1) % len(self.polygon)
            # mesh vertices
            point_x = self.polygon[i]
            point_y = self.polygon[j]
            # mesh edge
            self.circle_on_edge(point_x, point_y)

    def mesh_face(self):
        self.romb = find_romb(self.polygon)

        for point in self.romb:
            self.add_point(point)
        self.circle_inside(self.romb)

        self.pts_inside = np.array(self.pts_inside)
        self.pts_border = np.array(self.pts_border)
        self.velocities = np.array([[0.0, 0.0]]*(len(self.pts_inside) + len(self.pts_border)))

    def circle_on_edge(self, x, y):
        dist = euclidean(x, y)
#        print "x,y", x, y
#        print "r",uniform_size(x) + uniform_size(y)
#        print "dist", dist
        if (uniform_size(x) + uniform_size(y))/2.0 < dist:
#            print "new_point", dist
            new_point = (x + y)/2.
            if self.pts_border == [] or not np.any([(b == new_point).all() for b in self.pts_border]):
                # uuuh oneliner ale dziala
                self.pts_border.insert([i+1 for i,p in enumerate(self.pts_border) if (p==x).all()][0], new_point)
                self.circle_on_edge(x, new_point)
                self.circle_on_edge(new_point, y)

    def add_point(self,new_point):
        if point_in_poly(new_point, self.polygon):
            if self.pts_inside == [] or not np.any([(ins == new_point).all() for ins in self.pts_inside]):
                self.pts_inside.append(new_point)

    def circle_inside(self, rromb):
        good_to_go = True
        to_compare = [[0, 1], [1, 2], [2, 3], [1, 3]]
        for pair in to_compare:
            a = rromb[pair[0]]
            b = rromb[pair[1]]
            r_a = uniform_size(a) #change!
            r_b = uniform_size(b)
            dist = euclidean(a, b)
            if (r_a + r_b)/2.0 < dist:
                good_to_go = False
                break
        if not good_to_go:
            a = rromb[0]
            b = rromb[1]
            c = rromb[2]
            d = rromb[3]

            center = (a+c)/2.
            top = (a+b)/2
            right = (b+c)/2.
            bottom = (d+c)/2.
            left = (a+d)/2.

            self.add_point(center)
            self.add_point(top)
            self.add_point(right)
            self.add_point(bottom)
            self.add_point(left)

            romb_a = np.array([a, top, center, left])
            self.circle_inside(romb_a)
            romb_b = np.array([top, b, right, center])
            self.circle_inside(romb_b)
            romb_c = np.array([center, right, c, bottom])
            self.circle_inside(romb_c)
            romb_d = np.array([left, center, bottom, d])
            self.circle_inside(romb_d)


    def adapt_population(self):
        #print "adapt_population() started..."
#        print self.pts_inside
#        print self.pts_border
        allpts = None
        freeptscopy = self.pts_inside[:]
        if self.pts_inside != []:
            allpts = np.concatenate((freeptscopy, self.pts_border))
        else:
            allpts = self.pts_border
        itoremove = []
        ptoremove = []
        ptoadd = []

        for i in range(len(freeptscopy)):
            p = freeptscopy[i]
#            print p
#            print "allpts",allpts
            neighbours = [n for n in allpts if (euclidean(p,n)<=1.5*dist0(p,n) and (n != p).any())]
#            dodajemy tych ktorych juz bysmy dodali, zeby ratio nie bylo sztucznie zawyzone
#            already_added = [(p,n,euclidean(p,n),1.5*dist0(p,n)) for n in ptoadd if (euclidean(p,n)<=1.5*dist0(p,n) and (n != p).any())]
            already_added = [np.array(p) for n in ptoadd if (euclidean(p,n)<=1.5*dist0(p,n) and (n != p).any())]
            neighbours.extend( already_added )
#            print "already_to_add",len(already_added), already_added
            tmp = len(neighbours)#,neighbours
            # nie dodajemy tych, ktorzy juz sa straceni
            neighbours = filter(lambda x: ptoremove == [] or not np.any([(b == x).all() for b in ptoremove]), neighbours)
#            print "neighbours czystka, usunieto juz tyle:",tmp-len(neighbours)#,neighbours
            ratio = overlap_ratio(p,neighbours)
#            print "neighbours",neighbours
#            print "ratio ",ratio, p
            if ratio >= self.upper_ratio: #10.0:
                itoremove.append(i)
                ptoremove.append(p)
            if ratio <= self.lower_ratio: #4:
                angles = {line_angle(p,n):n for n in neighbours}
                max_angle =  -0.01
                ang1 = None
                ang2 = None
                sk = sorted(angles.keys())
                if len(sk) >= 2:
                    for i in xrange(len(sk)-1):
                        if ang_dist(sk[i+1],sk[i]) > max_angle:
                            max_angle = ang_dist(sk[i+1],sk[i])
                            ang1 = sk[i]
                            ang2 = sk[i+1]
                    if ang_dist(sk[0],sk[-1]) > max_angle:
                        max_angle = ang_dist(sk[0],sk[-1])
                        ang1 = sk[-1]
                        ang2 = sk[0]
                    ptdist = (euclidean(p,angles[ang1]) + euclidean(p,angles[ang2]))/2
                    m = max_angle/2 + ang1
                    px = p[0] + ptdist*math.cos(m)
                    py = p[1] + ptdist*math.sin(m)
                elif len(sk) < 2:
                    # point has <2 neighbours
                    if len(sk) == 1:
                        px = 2*p[0]-angles[sk[0]][0]
                        py = 2*p[1]-angles[sk[0]][1]
                    elif len(sk) == 0:
                        px = p[0] + uniform_size(p)
                        py = p[1] + uniform_size(p)
                ptoadd.append([px,py])

        arrtorem = np.array(itoremove)
        self.pts_inside = np.delete(freeptscopy,arrtorem,axis=0)
#        print "after deleting:",self.pts_inside
        arrtorem_v = arrtorem + len(freeptscopy)
        self.velocities = np.delete(self.velocities, arrtorem_v, axis=0)

        #print "deleted", len(itoremove), "bubbles"
        ptoadd = [p for p in ptoadd if point_in_poly(p,self.pts_border)]
        if len(ptoadd) > 0:
            arrtoadd = np.array(ptoadd)
            self.pts_inside = np.concatenate((self.pts_inside,arrtoadd))
            self.velocities = np.concatenate((self.velocities,[[0.0, 0.0]]*len(ptoadd)), axis=0)
        #print "added", len(ptoadd), "bubbles"
#        print "added bubbles:", ptoadd
        #print "adapt_population() ended"

        #return freepts

    def show_triangulation(self):
        #print "show_triangulation() started..."
        pts2 = map(lambda x: p2t.Point(x[0],x[1]), self.pts_border)
        cdt = p2t.CDT(pts2)
        for p in self.pts_inside:
            # print "ptptpt:",p[0],p[1]
            cdt.add_point(p2t.Point(p[0],p[1]))
        triangles= cdt.triangulate()
        pts3 = []
        triangles2 = []
        ind = 0
        for t in triangles:
            ti = []
            for p in [[t.a.x, t.a.y],[t.b.x, t.b.y],[t.c.x, t.c.y]]:
                if p not in pts3:
                    pts3.append(p)
                    ti.append(ind)
                    ind += 1
                else:
                    ti.append(pts3.index(p))
            triangles2.append(ti)
        pts3arr = np.array(pts3)
        x = pts3arr[:,0]
        y = pts3arr[:,1]
        #print "show_triangulation() ended..."
        plt.triplot(x,y,triangles2)


    def compute_bubbles_motion(self) :
        if len(self.pts_inside) == 0:
            points = self.pts_border
        else:
            points = np.concatenate((self.pts_border, self.pts_inside), axis=0)
        self.forces = [np.array([0.0, 0.0])] * len(points)
        for (ia,ib) in itertools.combinations(range(len(points)),2):
            (pa, pb) = (points[ia], points[ib])
            force = force_between(pa, uniform_size(pa), pb, uniform_size(pb))
            self.forces[ia] = self.forces[ia] + force
            self.forces[ib] = self.forces[ib] - force

    def move_bubbles(self):
        if len(self.pts_inside) == 0:
            points = self.pts_border
        else:
            points = np.concatenate((self.pts_border, self.pts_inside), axis=0)
        n_velos = self.next_velocities()
        n_points = self.next_points(points)
        self.velocities = n_velos
        in_border = len(self.pts_border)
#        self.pts_border = n_points[0:in_border]
        to_delete = []
        for (i,p) in enumerate(self.pts_border):
            p_n = self.pts_border[(i+1) % len(self.pts_border)]
            p_p = self.pts_border[(i-1) % len(self.pts_border)]
            in_poly = False
            for pp in self.polygon:
                if p[0] == pp[0] and p[1] == pp[1]:
                    in_poly = True
                    break
            if not in_poly:
                movement = []
                if p[0] == p_n[0]:
                    movement = np.array([0.0, n_points[i][1]-self.pts_border[i][1]])
#                    self.pts_border[i][1] = n_points[i][1]
                elif p[1] == p_n[1]:
                    movement = np.array([n_points[i][0]-self.pts_border[i][0], 0.0])
#                    self.pts_border[i][0] = n_points[i][0]
                else:
                    (an,bn) = straight_line(p, p_n)
                    p_move = n_points[i]
                    an_per = -(1.0/an)
                    bn_per = p[1] - an_per*p[0]
                    bn_move = p_move[1] - an*p_move[0]
                    x_cross = (bn_move - bn_per)/(an_per - an)
                    y_cross = an*x_cross + bn_move
                    p_cross = np.array([x_cross, y_cross])
                    movement = p_move - p_cross
#                    self.pts_border[i] = self.pts_border[i] + movement
                n_move = p_n - p
                p_move = p_p - p
                if((n_move[0] > 0 and movement[0] > 0) or (n_move[0] < 0 and movement[0] < 0)):
                    if math.fabs(movement[0]) < math.fabs(n_move[0]):
                        self.pts_border[i] = self.pts_border[i] + movement
                elif((n_move[1] > 0 and movement[1] > 0) or (n_move[1] < 0 and movement[1] < 0)):
                    if math.fabs(movement[1]) < math.fabs(n_move[1]):
                        self.pts_border[i] = self.pts_border[i] + movement
                elif((p_move[0] > 0 and movement[0] > 0) or (p_move[0] < 0 and movement[0] < 0)):
                    if math.fabs(movement[0]) < math.fabs(p_move[0]):
                        self.pts_border[i] = self.pts_border[i] + movement
                elif((p_move[1] > 0 and movement[1] > 0) or (p_move[1] < 0 and movement[1] < 0)):
                    if math.fabs(movement[1]) < math.fabs(p_move[1]):
                        self.pts_border[i] = self.pts_border[i] + movement
                elif not (movement[0] == 0.0 and movement[1] == 0.0):
                    print "DELETE!"
                    print movement
                    print p_move
                    print n_move
                    to_delete.append(i)
        self.pts_border = np.delete(self.pts_border, to_delete, axis=0)
        self.velocities = np.delete(self.velocities, to_delete, axis=0)

        self.pts_inside = n_points[in_border:len(n_points)]
        to_delete = []
        for i in range(len(self.pts_inside)):
            if not point_in_poly(self.pts_inside[i], self.pts_border):
                to_delete.append(i)
        to_delete = np.array(to_delete)
        self.pts_inside = np.delete(self.pts_inside, to_delete, axis=0)
        to_delete = to_delete + len(self.pts_border)
        self.velocities = np.delete(self.velocities, to_delete, axis=0)

    def next_velocities(self):
        return [self.velocities[i] + (self.forces[i] - self.friction*self.velocities[i])/self.mass * self.dt for i in range(len(self.forces))]

    def next_points(self, points):
        return [points[i] + self.velocities[i] * self.dt for i in range(len(points))]

#########################################################################
#                                                                       #
#  funkcje pomocnicze, ktore nie musza byc metodami, wyjalem co sie da  #
#                                                                       #
#########################################################################

def uniform_size(point):
    return 0.9
#    return math.fabs(point[0])/5 + 0.5


def point_in_poly(point, poly):
    n = len(poly)
    inside = False
    x = point[0]
    y = point[1]

    p1x = poly[0][0]
    p1y = poly[0][1]
    for i in range(n+1):
        p = poly[i % n]
        p2x = p[0]
        p2y = p[1]
        if y > min(p1y,p2y):
            if y < max(p1y,p2y):
                if x < max(p1x,p2x):
                    if p1y != p2y:
                        xinters = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                    if p1x == p2x or x < xinters:
                        inside = not inside
        p1x,p1y = p2x,p2y
    return inside

def find_romb(points):#, ax):
    maxY = sys.float_info.min
    minY = sys.float_info.max
    maxB = sys.float_info.min
    minB = sys.float_info.max

    for point in points:
        x = point[0]
        y = point[1]
        if y > maxY:
            maxY = y
        if y < minY:
            minY = y
        b = y - math.sqrt(3)*x
        if b > maxB:
            maxB = b
        if b < minB:
            minB = b

    cross = lambda y, b : np.array([(y-b)/math.sqrt(3), y])
    pLeftTop = cross(maxY, maxB)
    pLeftBottom = cross(minY, maxB)
    pRightTop = cross(maxY, minB)
    pRightBottom = cross(minY, minB)

    distA = euclidean(pLeftTop, pRightTop)
    distB = euclidean(pLeftBottom, pLeftTop)
    if distA > distB:
        diff = (distA - distB)/2.
        diffX = diff/2
        diffY = diff*math.sqrt(3)/2.
        pLeftTop[0] += diffX
        pLeftBottom[0] -= diffX
        pRightTop[0] += diffX
        pRightBottom[0] -= diffX
        pLeftTop[1] += diffY
        pLeftBottom[1] -= diffY
        pRightTop[1] += diffY
        pRightBottom[1] -= diffY
    elif distB > distA:
        diff = (distB - distA)/2.
        pLeftTop[0] -= diff
        pLeftBottom[0] -= diff
        pRightTop[0] += diff
        pRightBottom[0] += diff

    rectangle = np.array([pLeftTop, pRightTop, pRightBottom, pLeftBottom])
    #    polygon = mpatches.Polygon(rectangle, alpha=0.3)
    #    ax.add_patch(polygon)
    return rectangle


def dist0(pta,ptb):
    return 0.5*(uniform_size(pta)+uniform_size(ptb))

def line_through_pts(pta,ptb):
    x1 = pta[0]
    y1 = pta[1]
    x2 = ptb[0]
    y2 = ptb[1]
    return y2-y1,x1-x2,x2*y1-y2*x1

def line_orientation(line):
    a,b,c = line
    return math.atan2(a,-b)

def line_angle(pt, neigh):
    return line_orientation(line_through_pts(pt,neigh))

def norm_ang(x):
    '''
    zwraca znormalizowane wartosci danego kata do [0,2pi)
    '''
    #    while x >  math.pi: x -= 2*math.pi
    #    while x < -math.pi: x += 2*math.pi
    while x >=  2*math.pi:
        x -= 2*math.pi
    while x < 0:
        x += 2*math.pi
    return x

def ang_dist(u,v):
    diff = u-v
    return norm_ang(diff)

def overlap_ratio(point, neighs):
    ratio = 2/uniform_size(point)
#    print "fratio",ratio
#    print "neighs", neighs
    laps = [ uniform_size(point)+0.5*uniform_size(neigh)-euclidean(point,neigh) for neigh in neighs]
#    print "laps",laps
    return ratio*sum(laps)

def force_between(a_center, a_diameter, b_center, b_diameter, k=1):
    r0 = (a_diameter + b_diameter)/2
    r = euclidean(a_center, b_center)
    w = r/r0
    const = k/r0
    if w > 0 and w <= 1.5:
        f = 1.25*const*w*w*w - 2.375*const*w*w + 1.125*const
    elif w > 1.5:
        f = 0
    x = b_center[0] - a_center[0]
    y = b_center[1] - a_center[1]
    fx = f*x/r
    fy = f*y/r
    return -np.array([fx, fy])

def straight_line(pa, pb):
    x1 = pa[0]
    y1 = pa[1]
    x2 = pb[0]
    y2 = pb[1]
    a = (y1-y2)/(x1-x2)
    b = y1 - a*x1
    return (a,b)

