__author__ = 'Radek Lazarz'

import numpy as np
import bubble_mesh as bm
import sys

def main(poly):

    bubble_machine = bm.BubbleMesh()
    bubble_machine.set_poly(poly)
    bubble_machine.mesh()
    #bubble_machine.show()

def step_main(poly):


    bubble_machine = bm.BubbleMesh()
    bubble_machine.set_poly(poly)

    ended = False
    while not ended:
        # zaczekaj na nacisniecie entera
        ended = bubble_machine.next_step(adapt=True)
        if not ended: raw_input("Press Enter to continue...")
    raw_input("Press anything to exit...")


def get_alpaca():
    alpaca = np.array([[0, 2.5], [1, 0], [3, 0], [3, 3], [3.5, 4], [5, 3.25], [7, 3.5], [7, 0], [9, 0], [9, 3.75], [10, 4.5], [11.75, 10], [13, 11], [12, 12.5], [10, 12.5], [9, 9.75], [8, 8], [5, 8], [2.75, 8.75], [-0.25, 7]])

    #alpaca = np.array([[0,0], [a,0], [a,a], [0,a]])
    #alpaca = np.array([[-a,0],[-0.5*a,-s32*a],[0.5*a,-s32*a],[a,0],[0.5*a,s32*a],[-0.5*a,s32*a]])

    return alpaca

def get_hex():
    a = 3
    s32 = 1.732050812*0.5
    return np.array([[-a,0],[-0.5*a,-s32*a],[0.5*a,-s32*a],[a,0],[0.5*a,s32*a],[-0.5*a,s32*a]])

def get_poly(filename):
    fpts = []
    with open(filename) as f:
        lines = f.read().strip().split('\n')
        spts = [pt.strip().split(' ') for pt in lines]
        fpts = []
        for spt in spts:
            fpts.append([float(spt[0].strip()),float(spt[1].strip())])
    return np.array(fpts)


if __name__ == "__main__":
#    print "len(sys.argv)",len(sys.argv)
#    print "input args", sys.argv
    poly = get_poly(sys.argv[1]) if len(sys.argv) > 1 else get_hex()
    print "input polygon",poly

    #main(poly)
    step_main(poly)
